#Direct3D - Project 1: Drawing

##Summary
- This project is used to showcase the basics of drawing shapes using Direct3D.
- The main goal of this project is to follow and expand upon the lessons within Chapter 6 of Introduction to 3D Programming with DirectX 12.

##Contributer
- Christopher Rice

##References
- Introduction to 3D Programming with DirectX 12 by Frank D. Luna
  - The "Common" file is supplied from the textbook's GitHub and contains the framework required to initialize Windows and Direct3D.
