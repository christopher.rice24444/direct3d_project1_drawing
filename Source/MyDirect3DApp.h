#include "../Common/d3dApp.h"

// Custom Direct3D app class
class MyDirect3DApp : public D3DApp {
public:
	// Constructor for custom Direct3D app class
	MyDirect3DApp(HINSTANCE hInstance);

	// Destructor for custom Direct3D app class
	~MyDirect3DApp();

	// Initializes the Direct3D application
	virtual bool Initialize()override;

private:
	// Function called when window is resized
	virtual void OnResize()override;

	// Function called upon tick
	virtual void Update(const GameTimer& gt)override;

	// Function called to draw to the screen
	virtual void Draw(const GameTimer& gt)override;
};