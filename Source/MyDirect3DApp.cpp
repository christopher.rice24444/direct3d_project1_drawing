#include "MyDirect3DApp.h"
#include "../Common/d3dApp.h"
#include <DirectXColors.h>

using Microsoft::WRL::ComPtr;
using namespace DirectX;

// Custom vertex structure
struct Vertex {
	XMFLOAT3 Position;   // Position of vertex
	XMFLOAT4 Color;      // Color of vertex
};

// Constructor for custom Direct3D app class
MyDirect3DApp::MyDirect3DApp(HINSTANCE hInstance) : D3DApp(hInstance) {

}

// Destructor for custom Direct3D app class
MyDirect3DApp::~MyDirect3DApp() {

}

// Initializes the Direct3D application
bool MyDirect3DApp::Initialize() {
	if (!D3DApp::Initialize())
		return false;

	// Creating array of vertices for cube
	Vertex VertexArray[] = {
		{ XMFLOAT3(-1.0f, -1.0f, -1.0f), XMFLOAT4(Colors::White) },
		{ XMFLOAT3(-1.0f, +1.0f, -1.0f), XMFLOAT4(Colors::Black) },
		{ XMFLOAT3(+1.0f, +1.0f, -1.0f), XMFLOAT4(Colors::Red) },
		{ XMFLOAT3(+1.0f, -1.0f, -1.0f), XMFLOAT4(Colors::Green) },
		{ XMFLOAT3(-1.0f, -1.0f, +1.0f), XMFLOAT4(Colors::Blue) },
		{ XMFLOAT3(-1.0f, +1.0f, +1.0f), XMFLOAT4(Colors::Yellow) },
		{ XMFLOAT3(+1.0f, +1.0f, +1.0f), XMFLOAT4(Colors::Cyan) },
		{ XMFLOAT3(+1.0f, -1.0f, +1.0f), XMFLOAT4(Colors::Magenta) }
	};

	// The size of the vertex buffer to be created
	const UINT64 VertexBufferSize = 8 * sizeof(Vertex);

	// ComPtrs to the soon to be created vertex buffers (GPU and upload)
	ComPtr<ID3D12Resource> VertexBufferGPU = nullptr;
	ComPtr<ID3D12Resource> VertexBufferUpload = nullptr;

	// Creating the vertex buffer
	VertexBufferGPU = d3dUtil::CreateDefaultBuffer(md3dDevice.Get(), mCommandList.Get(), VertexArray, VertexBufferSize, VertexBufferUpload);

	// Creating vertex buffer view
	D3D12_VERTEX_BUFFER_VIEW VertexBufferView = { VertexBufferGPU->GetGPUVirtualAddress(), 8 * sizeof(Vertex), sizeof(Vertex) };

	// Creating an array of vertex buffer views
	D3D12_VERTEX_BUFFER_VIEW VertexBufferViewArray[] = { VertexBufferView };

	// Binding the vertex buffer view to the pipeline
	mCommandList->IASetVertexBuffers(0, 1, VertexBufferViewArray);

	// Setting the primitive topology
	mCommandList.Get()->IASetPrimitiveTopology(D3D_PRIMITIVE_TOPOLOGY_TRIANGLELIST);

	// Drawing the vertices in the vertex buffer
	mCommandList.Get()->DrawInstanced(8, 1, 0, 0);

	return true;
}

// Function called when window is resized
void MyDirect3DApp::OnResize() {
	D3DApp::OnResize();
}

// Function called upon tick
void MyDirect3DApp::Update(const GameTimer& gt) {

}

// Function called to draw to the screen
void MyDirect3DApp::Draw(const GameTimer& gt) {
	// Reuse the memory associated with command recording.
	// We can only reset when the associated command lists have finished execution on the GPU.
	ThrowIfFailed(mDirectCmdListAlloc->Reset());

	// A command list can be reset after it has been added to the command queue via ExecuteCommandList.
	// Reusing the command list reuses memory.
	ThrowIfFailed(mCommandList->Reset(mDirectCmdListAlloc.Get(), nullptr));

	// Indicate a state transition on the resource usage.
	mCommandList->ResourceBarrier(1, &CD3DX12_RESOURCE_BARRIER::Transition(CurrentBackBuffer(),
		D3D12_RESOURCE_STATE_PRESENT, D3D12_RESOURCE_STATE_RENDER_TARGET));

	// Set the viewport and scissor rect.  This needs to be reset whenever the command list is reset.
	mCommandList->RSSetViewports(1, &mScreenViewport);
	mCommandList->RSSetScissorRects(1, &mScissorRect);

	// Clear the back buffer and depth buffer.
	mCommandList->ClearRenderTargetView(CurrentBackBufferView(), Colors::LightSteelBlue, 0, nullptr);
	mCommandList->ClearDepthStencilView(DepthStencilView(), D3D12_CLEAR_FLAG_DEPTH | D3D12_CLEAR_FLAG_STENCIL, 1.0f, 0, 0, nullptr);

	// Specify the buffers we are going to render to.
	mCommandList->OMSetRenderTargets(1, &CurrentBackBufferView(), true, &DepthStencilView());

	// Indicate a state transition on the resource usage.
	mCommandList->ResourceBarrier(1, &CD3DX12_RESOURCE_BARRIER::Transition(CurrentBackBuffer(),
		D3D12_RESOURCE_STATE_RENDER_TARGET, D3D12_RESOURCE_STATE_PRESENT));

	// Done recording commands.
	ThrowIfFailed(mCommandList->Close());

	// Add the command list to the queue for execution.
	ID3D12CommandList* cmdsLists[] = { mCommandList.Get() };
	mCommandQueue->ExecuteCommandLists(_countof(cmdsLists), cmdsLists);

	// swap the back and front buffers
	ThrowIfFailed(mSwapChain->Present(0, 0));
	mCurrBackBuffer = (mCurrBackBuffer + 1) % SwapChainBufferCount;

	// Wait until frame commands are complete.  This waiting is inefficient and is
	// done for simplicity.  Later we will show how to organize our rendering code
	// so we do not have to wait per frame.
	FlushCommandQueue();
}